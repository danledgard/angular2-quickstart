Angular 2 quickstart tutorial

As detailed here https://angular.io/docs/js/latest/quickstart.html

Sinopia (at least version at Piksel) does not handle fetching scoped
packages from upstream repo. In order to be able to fetch angular 2
dependencies following registry config needs to be added

`npm config set @angular:registry https://registry.npmjs.org/`
